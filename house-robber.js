// You are a professional robber planning to rob houses along a street. 
// Each house has a certain amount of money stashed, 
// the only constraint stopping you from robbing each of them is that adjacent houses 
// have security systems connected and it will automatically contact the police 
// if two adjacent houses were broken into on the same night.

// Given an integer array nums representing the amount of money of each house, 
// return the maximum amount of money you can rob tonight without alerting the police.

// Example 1:

// Input: nums = [1,2,3,1]
// Output: 4
// Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
// Total amount you can rob = 1 + 3 = 4.
// Example 2:

// Input: nums = [2,7,9,3,1]
// Output: 12
// Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
// Total amount you can rob = 2 + 9 + 1 = 12.
 

// Constraints:

// 1 <= nums.length <= 100
// 0 <= nums[i] <= 400

// houses are indexes. We cannot use adjacent indexes. 0 excludes 1, 1 excludes 2. 
// We must skip in some way. 
// ? : Do we always have to start at index 0 at the beginning of our array? 
// [1] = arry of 1? rob 1
// [1,2] = pick highest = 2 
// [1,2,3] = which is higher? 1/3 or 2
// optimal is 1/3. How do we know whether to stick with it as the array gets bigger?
// is the sum of the next one higher than the current
// 
// As we go, we can see what the maximum as we've gone through.
// [1,2,3,4] 1/3, 2/4, 1/4 

// Let's look for a rule we can use

// A = input array | B is output array
// A =[2,7, 9, 3, 1, 15]
// B =[2,7, 11, 11, 12]

// [2]                          => B[0] = A[0]
// [2,7]                        => B[1] will be higher of A[0] or A[1]
// [2, 7, 11]                   => B[2] will be the higher of B[0] + A[2] || B[1] 
// [2, 7, 11, 11]               => B[3] will be higher of B[1] + A[3] || B[2] 
// [2, 7, 11, 11, 12]           => B[4] will be higher of B[2] + A[4] || B[3]
// [2, 7, 11, 11, 12, 26]       => B[5] will be higher of B[3] + A[5] || B[4] 

// if arr.length = 2 return highest
// if arr.length = 1 return

function optimalhousetheft(A) {

    if (A.length === 1) {
        return A[0]
    }

    let B = []
    B[0] = A[0]
    B[1] = Math.max(A[0], A[1])

    for (let i = 2; i < A.length; i++) {
        B.push(Math.max((B[i-2]+A[i]), B[i-1]))
    }

    return B[B.length - 1]
}

console.log(optimalhousetheft([2,7,9,3,100]))

// Notes about this solution:
// Saving the output array is not ideal use of space. 
// We can modify this slightly to only save previous and previous-previous
// E.g. we only need to keep B[1] until we're deciding between B[1] and B[2]
// So B[previous-previous, previous], for instance. 
// Another modification could be to wrap our comparator in a reduce function.
// This is concise but doesn't improve on our once-through solution considerably.
// That is, we're already at O(n) looping through our house array exactly once.
// With the array modification, we can have a space of O(2).
// It may not be possible to optimize the speed any further...
// We will always need to check all the elements in the array to compute the final sum.  