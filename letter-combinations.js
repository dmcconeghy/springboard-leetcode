 /* Solution by Dane
    
    head of tree is an empty string, branches off for each letter of the first digit (2: abc)
    
           //       ""
           //  /    |    \
           // "a"   "b"  "c"
    
    // each of those letters branch off to letters of the next digit (3: def)
    
      //             ""
      //        /    |    \
      //      "a"   "b"  "c"
      //    /  |  \
      // "ad" "ae" "af"      etc, etc
    
    // each digit represents a new "level" of the tree
    
    // begin with an empty string called combStr. build the string as we go down the tree
    
    // base case - when we reach a leaf... or when our combStr is as long as the string of digits
    // push the string into our results array and return (can't go farther down tree)
    
    // Keep track of what digit we are "on" with digitsIdx, and loop through all the letters of that digit
    
    // Add that letter to the combStr and attempt to continue down tree (to the next digit)
    
    // If we end up going "past" our lowest level, base case will trigger and loop will continue with the next letter of our current digit. 
    */

var letterCombinations = function(digits) {
    if (digits === "") return []
    let res = []
    
    let digitMap = {
        "2": "abc",
        "3": "def",
        "4": "ghi",
        "5": "jkl",
        "6": "mno",
        "7": "pqrs",
        "8": "tuv",
        "9": "wxyz"
    }
    
   
    
    function letterCombsHelper(digitsIdx, combStr) {
        if (combStr.length === digits.length) {
            res.push(combStr);
            return
        }
        for (let char of digitMap[digits[digitsIdx]]) {
            // combStr += char 
            // ***WILL NOT WORK (and we might be able to explain why...)
            let newCombStr = combStr + char
            letterCombsHelper(digitsIdx + 1, newCombStr)
        }
    }
    
    letterCombsHelper(0, "");
    return res;
};


/*

// this was the state of our group (Dane, Dave, Jeff) attempt before the early birds had to take flight. 

// Given a string containing digits from 2-9 inclusive, 
// return all possible letter combinations that the number could represent. 

// Return the answer in any order.

// A mapping of digit to letters (just like on the telephone buttons) is given below. 
// Note that 1 does not map to any letters.

// 2 abc
// 3 def
// 4 ghi
// 5 jkl
// 6 mno
// 7 pqrs
// 8 tuv
// 9 wxyz


// so if input is 2 we want it to return a, b, c
// then we want to push it to an array? 

// for each digit (letter, letter, letter)

// for each letter add it to the array

// 23
// abc def
// ["ad","ae","af","bd","be","bf","cd","ce","cf"]

// [[0][0], [0][1], [0][2], [1][0], [1][1], [1][2], [2][0], [2][1], [2][2]]

// for each num in 2 (abc) we need to check each num in 3 (def)

// 23
// for each number -> 2 // 3
// for each letter -> a
// for each number  3
// for each letter => d

// function gets characters abc
// then takes last character
// pushes it to res
// removes it from char
// then calls it with shorter characters 
// until list of characters is empty

// 23 
// () abc
// c
// [c]
// () ab
// b
// [c,b]
// () a
// a
// [c,b,a]
// then exit bc empty input

// start with var = ""
// recursive func that pushes letter to var for each digit
// input from our main function

// [abc][def][ghi]
// ai
// h

// [abc]
// a, b, c


// [abc][def][ghi]
// [fai.fbi.fci
// eai.ebi.eci
// dai.dbi.dci]

// [ghi [def [abc]]

// res = [] 
// loop over 2str => "abc"
// res = ["a", "b", "c"]
// loop over 3str => "def"
// res = ["ad", "bd", "cd", "ae", "be", "ce"]

// for each digit for its letters add them to the res
// for each next digit add its letters and all the letters of the digit that precedes 


// addDigitstoRes(){
//     add
// }

// */

// /**
//  * @param {string} digits
//  * @return {string[]}
//  */
//  var letterCombinations = function(digits) {

//     let res = []
    
//     let digitMap = {
//         "2": "abc",
//         "3": "def",
//         "4": "ghi",
//         "5": "jkl",
//         "6": "mno",
//         "7": "pqrs",
//         "8": "tuv",
//         "9": "wxyz"
//     }

//     function letterCombsHelper(nums, letterIdx, combStr) {
//         console.log(digits.length);
//         if (combStr.length === digits.length) {
//             res.push(combStr);
//         }
//         if (letterIdx === digitMap[nums[0]].length) {
//             nums = nums.substring(1);
//         }
//         if (nums.length === 0) return
//         combStr = combStr + digitMap[nums[0]].charAt(letterIdx)
//         console.log("combStr=", combStr)
//         letterCombsHelper(nums, letterIdx + 1, combStr);
//     }

//     // function getLetters(digits){

//     //         if(!digits) {
//     //             return res
//     //         }
//     //         console.log("sub", digits.substring(-1,1))
//     //         let letters = digitMap[digits.substring(-1,1)]
//     //         console.log("letters", letters)
//     //         res.push(recurseLetters(letters))
//     //         console.log("res", res)
//     //         let mod = digits.slice(0,digits.length - 1)
//     //         console.log("mod=", mod)
//     //         getLetters(mod)
        
//     // }

//     // function recurseLetters(str){
    
//     //     if (!str) {
//     //         return
//     //     }
//     //     let stack = str.split("")
 
//     //     let letter = stack.pop()
        
//     //     res.push(letter)
        
//     //     let stackString = stack.join("")
        
//     //     recurseLetters(stackString)

//     // }
//     // // console.log(recurseLetters("abc"))
//     // // getLetters("23")
//     // // console.log(res)

//     // getLetters(digits)
//     letterCombsHelper(digits, 0, "");
//     return res
// };

// console.log(letterCombinations("2"))

