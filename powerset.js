function recursion_powerset(array) {
    let output = [];
    recur([], 0)

    function recur(curr, index) {// index 0, 1, 2
      output.push(curr)
      for(let i = index; i < array.length; i++){
        recur([...curr, array[i]], i+1)
    }
 
 
  }
  console.log(output)
  return output
}

recursion_powerset([1,2,3])



