/**
 * Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

 

Example 1:

Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

Map: {
    abt: ["bat"],
    ant: ["nat", "tan"],
    aet: ["ate", "eat", "tea"]
}

Example 2:

Input: strs = [""]
Output: [[""]]

Example 3:

Input: strs = ["a"]
Output: [["a"]]
 

Constraints:

1 <= strs.length <= 104
0 <= strs[i].length <= 100
strs[i] consists of lowercase English letters.


?iterate over words, turn each word into a set, compare against array of sets

?Set("nat") !== Set("tan") ** won't work since they will not be equal either way

?Maybe create hashmap where key is sorted string and the value is an array of the words

create global {}; 

for each string, create variable = sorted string, check if in hashmap - if yes, add to array / if no, create new key value pair

Example 1:

Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

Map: {
    abt: ["bat"],
    ant: ["nat", "tan"],
    aet: ["ate", "eat", "tea"]
}

Loop through Map and push each array into an "output" array.

 */


var groupAnagrams = function(strs) {
    let map = {};
    for (let str of strs) {
        let sorted = str.split("").sort().join("");
        if (sorted in map) {
            map[sorted].push(str);
        } else {
            map[sorted] = [str];
        }
    }
    let output = [];
    for (let key in map) {
        output.push(map[key]);
    }
    return output;
};

// time complexity O(n*mlog(m)) where n is strs.length and m is the length of the strings in strs
// space O(n)