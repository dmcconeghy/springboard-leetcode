//  You are given an integer array coins representing coins of different denominations 
// and an integer amount representing a total amount of money.
//  Return the fewest number of coins that you need to make up that amount. 
// If that amount of money cannot be made up by any combination of the coins, return -1.
//  You may assume that you have an infinite number of each kind of coin.

//  Example 1:
 
//  Input: coins = [1,2,5], amount = 11
//  Output: 3
//  Explanation: 11 = 5 + 5 + 1
//  Example 2:
 
//  Input: coins = [2], amount = 3
//  Output: -1
//  Example 3:
 
//  Input: coins = [1], amount = 0
//  Output: 0
  
 
//  Constraints:
 
//  1 <= coins.length <= 12
//  1 <= coins[i] <= 231 - 1
//  0 <= amount <= 104

/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function(coins, amount) {

    if (target === 0) return 0;
    let A = new Array(target + 1).fill(-1);
    for (let i = 0; i < coins.length; i++){
        for (let j = coins[i]; j < target + 1; j++) {
            if (j === coins[i]) {
                A[j] = 1;
            } else if (A[j - coins[i]] === -1){
                continue
            } else {
                if (A[j] === -1){
                    A[j] = A[j - coins[i]] + 1;
                } else {
                    A[j] = Math.min(A[j - coins[i]] + 1, A[j]);
                }
            }
        }
    }
    return A[target];
};

/*
Initial thought was to do top-down.
E.g., [1, 5, 10] | [46]
Take 46/10 = 4.6; floor result is 4 
Then use biggest coin * num < target
So 4 10s. Then find remainder: 46-40 = 6 
6/5 = 1.2; floor result is 1
6-5=1 so 1 5. Then 1/1 = 1 and last 1 1 coin.

This works for many problems, but definitely not all of them.
Consider: [1,4,5] | 8
We might see that 8-5 is 3 and then try to use 3 1 coins. 
While that is a solution (5 + 1 + 1 + 1), it uses 4 coins. 
The correct solution is 4+4 or 2 coins. How do we know to check for this?
Writing the DFS backtrack for this might be an option.

Dane suggested instead looking bottom-up. 
So check each number and decide how many coins we need to make that up. 
[1,5] | [12]
[-1, 1, 2, 3, 4, 1, 2, 3, 4,  2,  3,  4] -> num of coins
[ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] -> (i) of value we can make

Similarly
[2,5] | [14]

2 only(a)    [-1,-1, 1,-1, 2,-1, 3,-1, 4,-1, 5, -1, 6, -1, 7]  -> initial num coins with only 2
add 5*coin(a)[-1,-1, 1,-1, 2, 1, 3, 2, 4, 3, 2,  4, 3,  5, 4]  -> modified num coins with 2 & 5 

Logic
if a[i-coin] != -1 then a[i] = Min(a[i-coin] + 1, a[i]) else don't change
We also need some logic so that -1 doesn't persist as Min
 or  
if a[i] === -1 then a[i] = a[i-coin] + 1 else don't change
*/

/*
    Summary: 
    1 - Create an empty array of length *amount* that is filled with -1. We'll call this array A
        Index of this array represents a 'target' value 
        and the value at that index represents the solution for that target
    2 - For each type of coin, loop through the array
        a - begin at index -> coin size (since that coin can't be used in any lower values... 
            $5 coin can't be used to make 1,2,3...)
        b - at index === coin, A[i] = 1 (if we have a $10 coin, $10 can be acheived with one coin)
        c - for all other indices, A[i] = Min(A[i - coin] + 1, A[i])
            UNLESS A[i] is currently = -1, in which case always set A[i] = A[i - coin] + 1
        d - return a[amount]

    Explanation of step 2C
        In C above, we look at the current target (on our way to our final amount)
        Then we decide between two choices:
            a) Our current target's starting index a[i] minus a[current coin's value] + 1 OR
            b) just a[i] 
                Take [2,5] | [10] First we check the 2s 
                    - When we're at index a[4] the value is 2 (2+2) 
                    - Why? A[4-2] tells us to look at index 2 which takes 1 coin for [a2].
                    - Then we just add the new coin we used to the amount of coins in a[2] 1+1=2
                    - At a[5] the value is -1 because we can't make 5 from 2s.
                    - At a[6] our index will be 6-2 or 4. num coins at a[4] is 2 + 1 more gives us a[6] = 3
                    - When we loop the second time for 5 we start at a[5]
                    - Since that == our coin, we set 1 as its num coins. 
                    - at a[7], however, we take 7-5 = a[2]. a[2] = 1 (a single 2 coin)
                    - then we know that a[7] takes two coins, one 2 and one 5. 
            c) If a[i] is still -1 after the first coin or multiple coins
                then prior coins could not make up this value. 
                So above, when we loop to a[7] its initial value is -1
                But with a 5 in the mix 7-5 = index 2 which works. 
*/