// Longest Substring Without Repeating Characters -- #3 : https://leetcode.com/problems/longest-substring-without-repeating-characters/

// Given a string s, find the length of the longest substring without repeating characters.

// Example 1:

// Input: s = "abcabcbb"
// Output: 3
// Explanation: The answer is "abc", with the length of 3.
// Example 2:

// Input: s = "bbbbb"
// Output: 1
// Explanation: The answer is "b", with the length of 1.
// Example 3:

// Input: s = "pwwkew"
// Output: 3
// Explanation: The answer is "wke", with the length of 3.
// Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
 

// Constraints:

// 0 <= s.length <= 5 * 10^4
// s consists of English letters, digits, symbols and spaces.

/**
 *  DISCUSSION
 * 
 * Would brute force just check all combinations? [i] 1-3 looking at max length. 
 * We could use sets to check each substring eg. 1, 12, 23, 123
 *  Can't mutate. 
 * 
 * What about a queue? If thing in is same as first in line, drop first in line? 
 * 
 * We could use a map? 
 * 
 * p pw pww
 * 
 * we iterate through, looking for max substring before we get a repeat. 
 * Then if we find a repeat we keep the max, but bump the iterator to the final repeated character. 
 * pwaw
 * 
 * [p, w, a, w,]
 * So enter queue, stay in queue until new repeated character forces everything before&including the old repeated character.
 * wwwwwww 
 * 
 */


/**
 * @param {string} s
 * @return {number}
 */
 var lengthOfLongestSubstring = function(s) {
    
    let queue = []
    let max = 0

    for (let character of s) {

        // index or -1 for not found
        let isDupe = queue.indexOf(character)
        
        if (isDupe !== -1){
            queue.splice(0,notdupe+1)
        } 
        
        queue.push(character)

        max = Math.max(max, queue.length)
       
    }

    return max

    

};

console.log(lengthOfLongestSubstring("pwwkew"))