/*
Level 1
The editor starts with a blank text document, with the cursor at initial position 0.

A) APPEND should append the inputted string text to the document starting from the current position of the cursor. 
After the operation, the cursor should be at the end of the added string.

queries = [
    ["APPEND", "Hey"],               | "" -> "Hey"
    ["APPEND", " there"],            | "Hey" -> "Hey there"
    ["APPEND", "!"]                  | "Hey there" -> "Hey there!"
]

// returns: [ "Hey",
//            "Hey there",
//            "Hey there!" ]

B) MOVE should move the cursor to the specified position. 
The cursor should be positioned between document characters, and are enumerated sequentially starting from 0. 
If the specified position is out of bounds, then move the cursor to the nearest available position.
queries = [
    ["APPEND", "Hey you"],           | "" -> "Hey you"
    ["MOVE", "3"],                   | moves the cursor after the first "y"
    ["APPEND", ","]                  | "Hey you" -> "Hey, you"
]

// returns: [ "Hey you",
//            "Hey you",
//            "Hey, you" ]

C) DELETE should remove the character right after the cursor, if any.
queries = [
    [APPEND", "Hello! world!"],      | "" -> "Hello! world!"
    ["MOVE", "5"],                   | moves the cursor before the first "!"
    ["DELETE"],                      | "Hello! world!" -> "Hello world!"
    ["APPEND", ","]                  | "Hello world!" -> "Hello, world!"
]

// returns: [ "Hello! world!",
//            "Hello! world!",
//            "Hello world!",
//            "Hello, world!" ]
and

queries = [
    ["APPEND", "!"],                 | "" -> "!"
    ["DELETE"]                       | "!" -> "!"
    ["MOVE", "0"],                   | moves the cursor before the first symbol
    ["DELETE"]                       | "!" -> ""
    ["DELETE"]                       | "" -> ""
]

// returns: [ "!",
//            "!",
//            "!",
//            "",
//            "" ]

Level 2 **************************************************************
Introduce methods to copy a part of the document text.

A) SELECT should select the text between the left and right cursor positions. 
Selection borders should be returned to the bounds of the document. 
If the selection is empty, it becomes a cursor position. 
Any modification operation replaces the selected text and places the cursor at the end of the modified segment.

For example, the following operations

queries = [
    ["APPEND", "Hello cruel world!"],  | "" -> "Hello cruel world!"
    ["SELECT", "5", "11"],             | selects " cruel"
    ["APPEND", ","]                    | "Hello cruel world!" -> "Hello, world!"
]

// returns: [ "Hello cruel world!",
//            "Hello cruel world!",
//            "Hello, world!" ]
produce "Hello, world!" with the cursor positioned after the comma.

SELECT and APPEND should replace the selected characters with the appended characters:

queries = [
    ["APPEND", "Hello"],               | "" -> "Hello"        
    ["SELECT", "2", "5"],              | Selects a substring "llo"
    ["APPEND", "y there"]              | "Hello" -> "Hey there"
]

// returns: [ "Hello",
//            "Hello",
//            "Hey there" ]

B) COPY should copy the selected text to the clipboard, if there is an active non-empty selection. 
The current selected text (if any) remains selected after the operation.

C) PASTE should append the text from the clipboard. 
The current selected text (if any) remains selected after the operation.
For example, the following operations

queries = [
    ["APPEND", "Hello, world!"],       | "" -> "Hello, world!"
    ["SELECT", "5", "12"],             | selects ", world"
    ["COPY"]                           | copies ", world"
    ["MOVE", "12"],                    | moves the cursor after "d"
    ["PASTE"],                         | "Hello, world!" -> "Hello, world, world!"
    ["PASTE"]                          | "Hello, world, world!" -> "Hello, world, world, world!"
]

// returns: [ "Hello, world!",
//            "Hello, world!",
//            "Hello, world!",
//            "Hello, world!",
//            "Hello, world, world!",
//            "Hello, world, world, world!" ]


*/



function solution(queries=null){
    let documentText = "";
    let cursorPos = 0;
    let selection = {value:"", start:0, end:0};
    let clipboard = "";
    let res = [];

    // const output = a.substr(0, position) + b + a.substr(position);

    function APPEND(str) {

        // Append no longer works with all cases.
        // Clipboard and selection having values can prevent successfull paste. 

        // if (!documentText){
        //     documentText += str
        //     cursorPos = documentText.length - 1;
        // } else if (str) {
        //     documentText = documentText.substring(0,cursorPos+1) + str + documentText.substring(cursorPos+1)
        //     cursorPos = documentText.length - 1;
        // }

        // if(selection.value !== ""){
        //     documentText = documentText.substring(0,selection.start) + str + documentText.substring(selection.end)
        // }
        // else if (!documentText){
        //     documentText += str
        //     cursorPos = documentText.length - 1;
        // } else {
        //     documentText = documentText.substring(0,cursorPos+1) + str + documentText.substring(cursorPos+1)
        //     cursorPos = documentText.length - 1;
        // }

        if (str === "" || !documentText) {
            documentText += str
            cursorPos = documentText.length - 1;
            console.log("append condition 1")

        } else if (str !== "" && documentText){
            documentText = documentText.substring(0,cursorPos+1) + str + documentText.substring(cursorPos+1)
            cursorPos = documentText.length - 1;
            console.log("append condition 2")

        } else if (selection.value !== ""){
            documentText = documentText.substring(0,selection.start) + str + documentText.substring(selection.end)
            console.log("append condition 3")
        }
         
        return documentText;
    }

    function MOVE(value) {
        if (cursorPos < 0) {
            cursorPos = 0;
        }

        if (cursorPos > documentText.length - 1) {
            cursorPos = documentText.length - 1;
        }
        
        cursorPos = +value -1;
        return documentText;
    }
    
    function DELETE(){
        
        documentText = documentText.slice(0, cursorPos +1) + documentText.slice(cursorPos+2, documentText.length);
        return documentText
    }
    
    function SELECT(startCursor, endCursor){
        selection.start = +startCursor;
        selection.end = +endCursor;
        selection.value = documentText.slice(startCursor, endCursor);
        console.log("SELECT", selection.value)

        if (selection.value === ""){
            cursorPos = endCursor;
        }
      
        return documentText

    }

    function COPY(){
        clipboard = selection.value;
        console.log('COPY' +clipboard)
        return documentText;
    }

    function PASTE(){
       
        console.log('PASTE' + clipboard)
        APPEND(clipboard)

        return documentText

    }
    for(let q of queries){

        if (q.length === 3){
            let [command, startCursor, endCursor] = q;

            if(command === "SELECT"){
                res.push(SELECT(startCursor, endCursor));
            }
        } else if (q.length === 2){
            let [command, value] = q;

            if(command === "APPEND"){
                res.push(APPEND(value));
            }
    
            if(command === "MOVE"){
                res.push(MOVE(value));
            }

        } else if (q.length === 1) {
            let [command] = q;

            if(command === "DELETE"){
            res.push(DELETE());
            }

            if(command === "PASTE"){
                res.push(PASTE())
            }

            if(command === "COPY"){
                res.push(COPY())
            }

        }
        
    }

    return res;
}

const queriesAPPEND = [["APPEND", "Hey"], ["APPEND", " there"], ["APPEND", "!"]];
const queriesMOVE = [["APPEND", "Hey you"], ["MOVE", "3"], ["APPEND", ","]];
const queriesMOVE2 = [["APPEND", "Hey you"], ["MOVE", "-10"], ["APPEND", "START "],["MOVE", "50"], ["APPEND", " END"]];
const queriesDELETE1 = [["APPEND", "Hello! world!"], ["MOVE", "5"], ["DELETE"], ["APPEND", ","]]
const queriesDELETE2 = [["APPEND", "!"], ["DELETE"], ["MOVE", "0"],  ["DELETE"], ["DELETE"]]
const queriesSELECT1 = [["APPEND", "Hello cruel world!"], ["SELECT", "5", "11"], ["APPEND", ","]]
const queriesSELECT2 = [["APPEND", "Hello"], ["SELECT", "2", "5"], ["APPEND", "y there"]]

const queriesCopyPaste = [
    ["APPEND", "Hello, world!"], 
    ["SELECT", "5", "12"], 
    ["COPY"], 
    ["MOVE", "12"], 
    ["PASTE"], 
    ["PASTE"]
]

console.log(solution(queriesAPPEND));
console.log(solution(queriesMOVE2));
console.log(solution(queriesDELETE1));
console.log(solution(queriesDELETE2));
console.log(solution(queriesSELECT1));
console.log(solution(queriesSELECT2));
console.log(solution(queriesCopyPaste));
