
// This demo code was taken from https://blog.bitsrc.io/depth-first-search-of-a-binary-tree-in-javascript-874701d8210a


class Node {
    constructor(val, left = null, right = null){
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
// These could be nested using the constructor's second and third arguments. 
// e.g., const nested = new Node(25, (new Node(14)), (new Node(35)))
const bst = new Node(27);
bst.left = new Node(14);
bst.left.left = new Node(10);
bst.left.left.left = new Node(6);
bst.left.left.right = new Node(8);
bst.left.right = new Node(19);
bst.left.right.left = new Node(13);
bst.left.right.right = new Node(18);
bst.right = new Node(35);
bst.right.left = new Node(31);
bst.right.left.left = new Node(23);
bst.right.left.right = new Node(26);
bst.right.right = new Node(42);
bst.right.right.left = new Node(37);
bst.right.right.right = new Node(40);

const dfsRecursivePreOrder = (node, arr = []) => {
    if(node) {
        arr.push(node.val);
        if(node.left) dfsRecursivePreOrder(node.left, arr);
        if(node.right) dfsRecursivePreOrder(node.right, arr);
    }
    // console.log(arr)
    return arr;
}

const preOrder = dfsRecursivePreOrder(bst)

const dfsRecursiveInOrder = (node, arr = []) => {
    if(node){
        if(node.left) dfsRecursiveInOrder(node.left, arr);
        arr.push(node.val);
        if(node.right) dfsRecursiveInOrder(node.right, arr);
    }
    // console.log(arr)
    return arr;
}

const inOrder = dfsRecursiveInOrder(bst)

const dfsRecursivePostOrder = (node, arr = []) => {
    if(node) {
        if(node.left) dfsRecursivePostOrder(node.left, arr);
        if(node.right) dfsRecursivePostOrder(node.right, arr);
        arr.push(node.val);
    }
    // console.log(arr)
    return arr;
}

const postOrder = dfsRecursivePostOrder(bst)

console.log(preOrder, inOrder, postOrder)

// Below you can see the iterative versions of these three recursive traversals

const dfsIterativePreorder = (root) => {
    let stack = [root]
    let traversed = [];
    let curr;

    while(stack.length){
        curr = stack.pop();
        traversed.push(curr.val)
        if(curr.right) stack.push(curr.right);
        if(curr.left) stack.push(curr.left);
    }

    return traversed;
}

// This version of Pre Order mirrors the In/Post Order traversals below
const dfsIterativePreorder2 = (root) => {
    const stack = [],
    traversed = [];
    let curr = root;

    while(stack.length || curr){
         while(curr) {
             traversed.push(curr.val)
             stack.push(curr);
             curr = curr.left;
         }
         curr = stack.pop()
         curr = curr.right;
    }
    
    return traversed;
}

const dfsIterateInOrder = (root) => {
    const stack = [],
    traversed = [];
    let curr = root;

    while(stack.length || curr){
        while(curr) {
            stack.push(curr);
            curr = curr.left;
        }
        curr = stack.pop()
        traversed.push(curr.val)
        curr = curr.right;        
    }

    return traversed;
}


// Includes two while loops
const dfsIterativePostorder = (root) => {  
    const s1 = [root],
    s2 = [],
    traversed = [];
    let curr;

    while (s1.length) {
        curr = s1.pop();
        if (curr.left) s1.push(curr.left);
        if (curr.right) s1.push(curr.right);
        s2.push(curr);
    }

    while (s2.length) {
        curr = s2.pop();
        traversed.push(curr.val);
    }

    return traversed;
}

// Includes one while loop, outputs reversed traversal stack
const dfsIterativePostorder2 = (root) => {  
    const s1 = [root],
    s2 = [];
    let curr;

    while (s1.length) {
        curr = s1.pop();
        if (curr.left) s1.push(curr.left);
        if (curr.right) s1.push(curr.right);
        s2.push(curr.val);
    }

    return s2.reverse();
}
